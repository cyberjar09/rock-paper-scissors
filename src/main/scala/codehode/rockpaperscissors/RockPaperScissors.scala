package codehode.rockpaperscissors

import scala.io.StdIn
import scala.util.Random

object RockPaperScissors extends Gameplay with App {
  do {
    val cpuChoice = getChoice(Random.nextInt(3))
    val humanChoice = getChoice(StdIn.readLine("0: Rock, 1: Paper, 2: Scissors\nInput:"))
    getResult(cpuChoice, humanChoice)

  } while(userWantsToPlay)
}

trait Gameplay {
  def userWantsToPlay: Boolean = {
    val wantsToPlay = StdIn.readLine("Play again?")
    wantsToPlay.matches("(?i)y|yes")
  }

  def getChoice(i: Int): Choice = getChoice(i.toString)

  def getChoice(x: String): Choice = {
    x match {
      case "0" => Rock
      case "1" => Paper
      case "2" => Scissors
      case _ => throw new IllegalArgumentException("Please only input a value between [0,1,2]")
    }
  }

  def getResult(cpuChoice: Choice, humanChoice: Choice) = {
    if (humanChoice == cpuChoice) println("It's a tie!")
    else humanChoice match {
      case Rock => cpuChoice match {
        case Paper => println(s"$Paper covers $Rock, CPU wins!")
        case Scissors => println(s"${Rock} smashes ${Scissors}, you win!")
      }
      case Paper => cpuChoice match {
        case Rock => println(s"$Paper covers $Rock, you win!")
        case Scissors => println(s"$Scissors cuts $Paper, CPU wins!")
      }
      case Scissors => cpuChoice match {
        case Rock => println(s"$Rock smashes $Scissors, CPU wins!")
        case Paper => println(s"$Scissors cuts $Paper, you win!")
      }
    }
  }
}

sealed trait Choice
case object Rock extends Choice
case object Paper extends Choice
case object Scissors extends Choice